---
layout: page
title: 3D-printer
author: Sander
date: 2021-07-06
topics: [3d-printing, designing]
wip: true
image: /uploads/images/3d-printer/ultimaker-s3.png
---
We hebben twee [Ultimaker S3](https://ultimaker.com/nl/3d-printers/ultimaker-s3) 3D-printers waar we gebruik van mogen maken.

![](/uploads/images/3d-printer/ultimaker-s3.png)

Het maximale werkoppervlak van de Ultimaker S3 is 230 x 190 x 200 mm.
Iedere Ultimaker S3 heeft twee extruders en kan met twee fillamenten tegelijkertijd printen.

Ook is er [Ultimaker PVA](https://ultimaker.com/nl/materials/pva) (polyvinylalcohol) beschikbaar. Met dit in water oplosbaar support materiaal kan je complexe printjes maken.

Het makkelijkste programma om je ontwerpen en STL bestanden in te slicen, is [Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura).

![](/uploads/images/3d-printer/cura.png)
