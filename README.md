# Wiki
This is The Space Leiden wiki POC
Posts, Projects and tooling are added by creating a new markdown page in the folder

## Run wiki locally
If you want you can checkout this repo and run the wiki locally for development purposes.

### Docker
You can use a pre-built docker image from jekyll:
```shell
docker run -v $(pwd):/srv/jekyll -p 4000:4000 -it jekyll/jekyll:4.2.0 jekyll serve

# visit http://localhost:4000
```

### Ubuntu / Debian / WSL
Or install ruby locally on Ubuntu/Debian/WSL:
```shell
sudo apt install gnupg2

# install Ruby version manager RVM (https://rvm.io)
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
source /home/user/.rvm/scripts/rvm

# install the latest ruby version and bundler
rvm install ruby --latest

gem install bundler
bundle config set --local path 'vendor/bundle'
bundle install

# run jekyll on http://localhost:4000
bundle exec jekyll serve
```

### MacOS
On MacOS:
```shell
brew install ruby
gem install bundler

# install dependencies locally to prevent pollution of your machine
bundle config set --local path 'vendor/bundle'
bundle install

# run jekyll on http://localhost:4000
bundle exec jekyll serve
```
