---
layout: page
title: Verbinden met de Space VPN
author: Sander
date: 2021-08-26
topics: [networking]
image: /uploads/images/vpn/wireguard.png
---

Je kan met het LAN van de Space verbinden via een VPN verbinding.
Hiervoor zal je een Wireguard client op je machine moeten installeren en een
paar stappen doorlopen.

## VPN verbinding opzetten

### 1. Kies een Wireguard client
Op [de website van Wireguard](https://www.wireguard.com/install/) zijn een hoop clients uit een gezet.
Kies er een, volg de instructies voor jouw OS en ga dan verder.

### 2. Genereer een keypair
Maak een nieuwe wireguard keypair aan. Dit kan je in de GUI van je Wireguard
client doen, of via de CLI:

```shell
wg keygen > spacevpn.key
wg pubkey < spacevpn.key > spacevpn.pub
```

### 3. Vraag om toegang met je public key
Een van de Space IT infra beheerders moet nu je public key toevoegen aan de
Wireguard server. Stuur bijvoorbeeld een berichtje op Discord of IRC.

### 4. Fix je tunnel configuratie
Hier onder zie je een voorbeeld van een tunnel configuratie die je kan gebruiken
om met je client te verbinden met de Wireguard VPN server.
Vervang `your_private_key` en `your_static_ip` met de inhoud van jouw `spacevpn.key` private key en
het statische IP adres dat je toegewezen hebt gekregen van een van de beheerders.


```toml
[Interface]
PrivateKey = your_private_key
Address = your_static_ip

[Peer]
PublicKey = SwFW2uppVYRPrcrx108PgW3Cf5xcj4340G7BGetQh2c=
AllowedIPs = 10.3.14.0/24
Endpoint = stargate.spaceleiden.nl:13337
PersistentKeepalive = 25
```

## Jumphost gebruiken
Je kan nu praten met hosts binnen het `10.3.14.0/24` subnet. Je kan nu bijvoorbeeld
via ssh verbinden met de jumphost (`10.3.14.2`) om vervolgens te praten met een VM binnen `spaceleiden.local` en het `10.13.37.0/24` subnet.
